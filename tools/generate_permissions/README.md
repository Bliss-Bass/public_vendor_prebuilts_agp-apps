# Generate Permissions

This script will generate a permissions xml for apk files. 

### Instructions:

Place the apk files in the bin/ folder

Run the script:

`bash generate_perms.sh`

Your permissions.xml will show up in the permissions/ folder
